﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThrdsApp
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            m_SpnrTasksToRun.Value = 6;
            m_SpnrTimePerTask.Value = 1;
        }

        private void OnRunTasksClicked(object sender, EventArgs e)
        {
            int iTasksToRun = (int)m_SpnrTasksToRun.Value;
            int iTimePerTask = (int)m_SpnrTimePerTask.Value;
            if (iTasksToRun < 1 || iTimePerTask < 1)
                return;
            TaskMonitor taskMonitor = new TaskMonitor(iTasksToRun, iTimePerTask);
            TaskMonitor.TaskCompletedDelegate taskCompletedHandler = null;
            taskCompletedHandler = (iTaskNumber) =>
             {
                 if (iTaskNumber+1 == taskMonitor.Tasks) 
                 {
                     taskMonitor.TaskCompleted -= taskCompletedHandler;
                     if (this.InvokeRequired)
                     {
                         this.BeginInvoke(new Action(() => m_CmdRunTasks.Enabled = true));
                     }
                     else
                     {
                         m_CmdRunTasks.Enabled = true;
                     }
                 }
             };
            taskMonitor.TaskCompleted += taskCompletedHandler;
            new ProgressForm(taskMonitor).Show(this);
            m_CmdRunTasks.Enabled = false;
            taskMonitor.Run();
        }
        private void update()
        {

        }
        private void OnExitClicked(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

    }

    public class TaskMonitor
    {
        private int m_iTimePerTask = 0;
        private bool m_bRunAsync = true;

        public delegate void TaskCompletedDelegate(int iTaskNumber);
        public event TaskCompletedDelegate TaskCompleted = delegate { };
        public TaskMonitor(int iTasksToRun, int iTimePerTask)
        {
            m_iTimePerTask = iTimePerTask;
            Tasks = iTasksToRun;
        }
        public int Tasks { get; private set; }
        public void Run()
        {
            if (m_bRunAsync)
            {
                //Run this on another thread...
                new System.Threading.Tasks.Task(() =>
                {
                    runTasks();
                }).Start();
            }
            else
            {
                runTasks();
            }
        }
        private void runTasks()
        {
            for (int i = 0; i < Tasks; ++i)
            {
                System.Threading.Thread.Sleep(m_iTimePerTask * 1000);
                TaskCompleted(i);
            }
        }
    }
}
