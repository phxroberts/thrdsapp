﻿namespace ThrdsApp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.m_CmdRunTasks = new System.Windows.Forms.Button();
            this.m_CmdExit = new System.Windows.Forms.Button();
            this.m_SpnrTasksToRun = new System.Windows.Forms.NumericUpDown();
            this.m_SpnrTimePerTask = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.m_SpnrTasksToRun)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_SpnrTimePerTask)).BeginInit();
            this.SuspendLayout();
            // 
            // m_CmdRunTasks
            // 
            this.m_CmdRunTasks.Location = new System.Drawing.Point(28, 146);
            this.m_CmdRunTasks.Name = "m_CmdRunTasks";
            this.m_CmdRunTasks.Size = new System.Drawing.Size(98, 23);
            this.m_CmdRunTasks.TabIndex = 0;
            this.m_CmdRunTasks.Text = "Run Tasks";
            this.m_CmdRunTasks.UseVisualStyleBackColor = true;
            this.m_CmdRunTasks.Click += new System.EventHandler(this.OnRunTasksClicked);
            // 
            // m_CmdExit
            // 
            this.m_CmdExit.Location = new System.Drawing.Point(161, 146);
            this.m_CmdExit.Name = "m_CmdExit";
            this.m_CmdExit.Size = new System.Drawing.Size(98, 23);
            this.m_CmdExit.TabIndex = 1;
            this.m_CmdExit.Text = "Exit";
            this.m_CmdExit.UseVisualStyleBackColor = true;
            this.m_CmdExit.Click += new System.EventHandler(this.OnExitClicked);
            // 
            // m_SpnrTasksToRun
            // 
            this.m_SpnrTasksToRun.Location = new System.Drawing.Point(175, 37);
            this.m_SpnrTasksToRun.Name = "m_SpnrTasksToRun";
            this.m_SpnrTasksToRun.Size = new System.Drawing.Size(75, 22);
            this.m_SpnrTasksToRun.TabIndex = 2;
            // 
            // m_SpnrTimePerTask
            // 
            this.m_SpnrTimePerTask.Location = new System.Drawing.Point(175, 89);
            this.m_SpnrTimePerTask.Name = "m_SpnrTimePerTask";
            this.m_SpnrTimePerTask.Size = new System.Drawing.Size(75, 22);
            this.m_SpnrTimePerTask.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tasks To Run:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Time Per Task:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 201);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.m_SpnrTimePerTask);
            this.Controls.Add(this.m_SpnrTasksToRun);
            this.Controls.Add(this.m_CmdExit);
            this.Controls.Add(this.m_CmdRunTasks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Task Runner";
            ((System.ComponentModel.ISupportInitialize)(this.m_SpnrTasksToRun)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.m_SpnrTimePerTask)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button m_CmdRunTasks;
        private System.Windows.Forms.Button m_CmdExit;
        private System.Windows.Forms.NumericUpDown m_SpnrTasksToRun;
        private System.Windows.Forms.NumericUpDown m_SpnrTimePerTask;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

