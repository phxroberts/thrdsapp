﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThrdsApp
{
    public partial class ProgressForm : Form
    {

        public ProgressForm(TaskMonitor taskMonitor)
        {
            InitializeComponent();

            m_ProgressBar.Maximum = taskMonitor.Tasks;

            TaskMonitor.TaskCompletedDelegate taskCompletedHandler = null;
            taskCompletedHandler = (iTaskNumber) =>
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new Action(() => updateProgress(iTaskNumber, taskMonitor.Tasks)));
                }
                else
                {
                    updateProgress(iTaskNumber, taskMonitor.Tasks);
                }
                if (iTaskNumber == taskMonitor.Tasks)
                {
                    taskMonitor.TaskCompleted += taskCompletedHandler;
                }
            };
            taskMonitor.TaskCompleted += taskCompletedHandler;
            
        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
        }
        private void updateProgress(int iTaskNumber, int iTotalTasks)
        {
            m_ProgressBar.Increment(1);
        }
    }
}
